FROM python:3.6

ENV DEBIAN_FRONTEND noninteractive

COPY . /app
WORKDIR /app

#RUN apk update && apk add --no-cache gcc musl-dev
RUN apt-get update && apt-get install gcc

RUN pip install -r requirements.txt

EXPOSE 8506

CMD ["/app/start.sh"]