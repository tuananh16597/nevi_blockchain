#!/usr/bin/env python3
import sys, os, time
from web3 import Web3, HTTPProvider
import json
from pathlib import Path

PROJECT_ROOT = os.path.dirname(os.path.dirname(__file__))

def bytearray2hexstring(inputArray):
    tmpstr = ''.join(format(x, '02x') for x in inputArray)
    return "0x" + tmpstr


def load_bcNodeProfileFile(profileDirPath):
    profiles = os.listdir(profileDirPath)
    returnCfg = {}
    for profile in profiles:
        returnCfg.update({profile: {}})
        with open(profileDirPath + '/' + profile) as fp:
            line = fp.readline()
            cnt = 1
            while line:
                # print(line)
                [key, value] = line.split('=')
                returnCfg[profile].update({key: value.replace('"', '').rstrip()})
                line = fp.readline()
                cnt += 1
    return returnCfg


if __name__ == '__main__':
    confFile = Path(os.path.join(PROJECT_ROOT, 'conf.bin'))
    cfg = {}
    if confFile.exists():
        json_data = open('conf.bin').read()
        cfg = json.loads(json_data)
    else:
        print(confFile)

    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'blchain.settings')
    from blchain import settings

    settings.BLOCKCHAIN_CONFIG_DATA = cfg
    settings.ALLOWED_HOSTS = cfg['webServer_publicIP']
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)
