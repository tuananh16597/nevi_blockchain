from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.template.loader import get_template
from django import template
import json
import traceback
import sys, os, time

from pathlib import Path

from random import randint
from web3 import Web3, HTTPProvider
from web3.utils.events import get_event_data

PROJECT_ROOT = os.path.dirname(os.path.dirname(__file__))


def bytearray2hexstring(inputArray):
    tmpstr = ''.join(format(x, '02x') for x in inputArray)
    return "0x"+tmpstr

def index(request):
    template = get_template('blNodeGUI_central.html')
    result = {}

    context = result
    return HttpResponse(template.render(context, request))

def ajax_BCUserInfoQuery(request):
    if request.is_ajax():
        # Test data
        # data = "{'bcUserDetail': [{'userIdent': 'robot', 'userName': 'Robot', 'userImage': 'http://115.78.1.103:8504/jackal.png', 'balance': '46150.00', 'blocknumber': 9230, 'provide': [], 'comsume': [], 'purchasedAppList': [[1, '1', '1', '1', 'http://115.78.1.103:8504/unknown.png']]}, {'userIdent': 'dataProvider', 'userName': 'Data provider', 'userImage': 'http://115.78.1.103:8504/data_provider.png', 'balance': '46005.00', 'blocknumber': 9201, 'provide': [], 'comsume': [], 'purchasedAppList': []}], 'applist': [[1, '0x45b75acBD0bD29e23f2Ac9B736B389746193ed24', 1, '1', '1', '1', 'http://115.78.1.103:8504/unknown.png', 1, 'robot'], [2, '0x45b75acBD0bD29e23f2Ac9B736B389746193ed24', 100000, 'Data 1', 'Hash', 'url', 'http://115.78.1.103:8504/unknown.png', 0, 'robot']]}"
        # return HttpResponse(json.dumps(eval(data)))
        # End test data
        try:
            result = {
                'bcUserDetail'  : [],
                'applist'       : []
            }

            confFile = Path(os.path.join(PROJECT_ROOT, 'conf.bin'))
            cfg={}
            if confFile.exists():
                json_data=open('conf.bin').read()
                cfg = json.loads(json_data)
                #for key, user in cfg['userInfo'].items():
                #    minerRun(user)
            bcConf = cfg
            dataNameIcon_Map=bcConf["iconMap"]
            ipMedia=bcConf['bcNodeProfiles']['generalprofile']['resServer_ipAddress']
            portMedia = bcConf['bcNodeProfiles']['generalprofile']['resServer_port']
            #print(bcConf)
            tmpuserBCDict = {}
            for userKey, userInfo in bcConf['userInfo'].items():
                #print(userKey, userInfo)
                if(userInfo['account']==''):
                    continue
                transactionTarget = userInfo['link']
                transactionAccount = userInfo['account']
                transactionPassPhrase = userInfo['passPhrase']
                #print(transactionTarget, transactionAccount)
                try:
                    userBCDetail = {}
                    w3 = Web3(Web3.HTTPProvider(transactionTarget, request_kwargs={'timeout': 60}))
                    w3.eth.defaultAccount = transactionAccount
                    userBCDetail.update({
                        'userIdent'     : userInfo['ident'],
                        'userName'      : userInfo['name'],
                        'userImage'     : "http://"+ipMedia+":"+portMedia+"/"+dataNameIcon_Map[userInfo['ident']],
                        'balance'       : str("{0:.2f}".format(round(w3.fromWei(w3.eth.getBalance(transactionAccount),'ether'),2))),
                        'blocknumber'   : w3.eth.blockNumber,
                        'provide'       : [],
                        'comsume'       : []
                    })
                    userBCDetail.update({'purchasedAppList':[]})
                    tmpuserBCDict.update({userInfo['account']:userBCDetail})
                except Exception as e:
                    traceback.print_exc()
                    continue
            #print(tmpuserBCDict)
            if 'contract' in bcConf:
                transactionTarget = bcConf['userInfo'][bcConf['commonUse_user']]['link']
                transactionAccount = bcConf['userInfo'][bcConf['commonUse_user']]['account']
                transactionPassPhrase = bcConf['userInfo'][bcConf['commonUse_user']]['passPhrase']
                contractAddress = bcConf['contract']['address']
                contractABI = bcConf['contract']['abi']

                w3 = Web3(Web3.HTTPProvider(transactionTarget, request_kwargs={'timeout': 60}))
                contractInstance = w3.eth.contract(
                    address=contractAddress,
                    abi=contractABI,
                )
                logFormatDict = {}
                for interfaceFormat in contractABI[:]:
                    logFormatDict.update({interfaceFormat['name']:interfaceFormat})

                w3.personal.unlockAccount(account=transactionAccount,passphrase=transactionPassPhrase)
                index = 0 #traverse from list's starting point
                appList = []
                while True:
                    try:
                        [nextIndex, payee, price, dataName, dataHash, dataUrl, dataTotal] = contractInstance.functions.getNextDataRecord(index).call()
                    except Exception as e:
                        #traceback.print_exc()
                        break
                    if nextIndex != 0:
                        if dataName.decode('UTF-8') not in dataNameIcon_Map:
                            dataImageUrl = "http://"+ipMedia+":"+portMedia+"/"+dataNameIcon_Map['unknown']
                        else:
                            dataImageUrl = "http://"+ipMedia+":"+portMedia+"/"+dataNameIcon_Map[dataName.decode('UTF-8')]
                        dataPayeeIdent = None
                        for uKey, uInfo in bcConf['userInfo'].items():
                            if(uInfo['account']==payee):
                                dataPayeeIdent = uInfo['ident']
                                break
                        appList.append([nextIndex, payee, price, dataName.decode('UTF-8'), dataHash.decode('UTF-8'), dataUrl.decode('UTF-8'),dataImageUrl, dataTotal, dataPayeeIdent])
                    else:
                        break
                    index = nextIndex
                result['applist']=(appList)
                for userKey, userInfo in bcConf['userInfo'].items():
                    transactionTarget = userInfo['link']
                    transactionAccount = userInfo['account']
                    transactionPassPhrase = userInfo['passPhrase']
                    try:
                        userBCDetail = tmpuserBCDict[userInfo['account']]
                        w3 = Web3(Web3.HTTPProvider(transactionTarget, request_kwargs={'timeout': 60}))
                        contractInstance = w3.eth.contract(
                            address=contractAddress,
                            abi=contractABI,
                        )
                        logFormatDict = {}
                        for interfaceFormat in contractABI[:]:
                            logFormatDict.update({interfaceFormat['name']:interfaceFormat})
                        w3.eth.defaultAccount = transactionAccount

                        w3.personal.unlockAccount(account=w3.eth.defaultAccount,passphrase=transactionPassPhrase)
                        index = 0 #traverse from list's starting point
                        purchasedAppList = []
                        while True:
                            try:
                                [nextIndex, dataName, dataHash, dataUrl, dataPayee] = contractInstance.functions.getNextAccessibleDataFromPurchaser(index, userInfo['account']).call()
                            except Exception as e:
                                #traceback.print_exc()
                                break;
                            if nextIndex != 0:
                                if dataName.decode('UTF-8') not in dataNameIcon_Map:
                                    dataImageUrl = "http://"+ipMedia+":"+portMedia+"/"+dataNameIcon_Map['unknown']
                                else:
                                    dataImageUrl = "http://"+ipMedia+":"+portMedia+"/"+dataNameIcon_Map[dataName.decode('UTF-8')]

                                purchasedAppList.append([nextIndex, dataName.decode('UTF-8'), dataHash.decode('UTF-8'), dataUrl.decode('UTF-8'),dataImageUrl])
                            else:
                                break
                            index = nextIndex
                        userBCDetail.update({'purchasedAppList':purchasedAppList})
                    except Exception as e:
                        #traceback.print_exc()
                        continue
            for useraccount, userInfo in tmpuserBCDict.items():
                result['bcUserDetail'].append(userInfo)
        except Exception as e:
            traceback.print_exc()
            return HttpResponse(json.dumps({'result': 'Error'})) # incorrect post
        return_data = result
        return HttpResponse(json.dumps(return_data))
    else:
        raise Http404

def ajax_dcUserRegister(request):
    if request.is_ajax():

        return_data = {
            'result': [],
        }
        print(json.dumps(return_data))
        return HttpResponse(json.dumps(return_data))
    else:
        raise Http404

def ajax_dataRegister(request):
    if request.is_ajax():
        result = False
        try:
            selectedParticipant =  request.POST.get('selectedParticipant', False); #request.POST['selectedParticipant']
            dataName            =  request.POST.get('dataNameReg', False); #request.POST['dataNameReg']
            dataHash            =  request.POST.get('dataHashReg', False); #request.POST['dataHashReg']
            dataUrl             =  request.POST.get('dataUrlReg', False); #request.POST['dataUrlReg']
            dataPrice           =  request.POST.get('dataPriceReg', False); #request.POST['dataPriceReg']
            confFile = Path(os.path.join(PROJECT_ROOT, 'conf.bin'))
            cfg={}
            if confFile.exists():
                json_data=open('conf.bin').read()
                cfg = json.loads(json_data)
            bcConf = cfg
            dataNameIcon_Map=bcConf["iconMap"]
            ipMedia=bcConf['bcNodeProfiles']['generalprofile']['resServer_ipAddress']
            portMedia = bcConf['bcNodeProfiles']['generalprofile']['resServer_port']

            for userKey, userInfo in bcConf['userInfo'].items():
                if(userInfo['ident']==selectedParticipant):
                    try:
                        transactionTarget = userInfo['link']
                        transactionAccount = userInfo['account']
                        transactionPassPhrase = userInfo['passPhrase']
                        contractAddress = bcConf['contract']['address']
                        contractABI = bcConf['contract']['abi']

                        w3 = Web3(Web3.HTTPProvider(transactionTarget, request_kwargs={'timeout': 60}))
                        w3.eth.defaultAccount = transactionAccount
                        w3.personal.unlockAccount(account=transactionAccount,passphrase=transactionPassPhrase)

                        contractInstance = w3.eth.contract(
                            address=contractAddress,
                            abi=contractABI,
                        )
                        logFormatDict = {}
                        for interfaceFormat in contractABI[:]:
                            logFormatDict.update({interfaceFormat['name']:interfaceFormat})

                        tx_hash = contractInstance.functions.dataRegister(transactionAccount, int(dataPrice), bytes(dataName,'utf-8'), bytes(dataHash,'utf-8'), bytes(dataUrl,'utf-8')).transact()
                        receipt = w3.eth.waitForTransactionReceipt(tx_hash)
                        logs = contractInstance.events.datareg_event().processReceipt(receipt)
                        try:
                            result = logs[0].args.datareg
                        except Exception:
                            print("Fail to execute Data Registering function (tx_hash = 0x",bytearray2hexstring(tx_hash)," )")
                            traceback.print_exc()
                            result = False
                        print("********************************************************")
                        if result:
                            print("Software had been registered")
                        else:
                            print("Failed to register Software")
                        print("********************************************************")
                        break
                    except Exception:
                        traceback.print_exc()
                        result = False
        except Exception:
            traceback.print_exc()
            return HttpResponse(json.dumps({'result': 'Error'})) # incorrect post
        return_data = {
            'result': result,
        }
        print(json.dumps(return_data))
        return HttpResponse(json.dumps(return_data))
    else:
        raise Http404

def ajax_dataPurchase(request):
    if request.is_ajax():
        try:
            selectedParticipant =  request.POST.get('selectedParticipant', False); #request.POST['selectedParticipant']
            dataName    =  request.POST['dataName']
            print("ajax_dataPurchase request on : ", dataName, selectedParticipant)

            confFile = Path(os.path.join(PROJECT_ROOT, 'conf.bin'))
            cfg={}
            if confFile.exists():
                json_data=open('conf.bin').read()
                cfg = json.loads(json_data)
            bcConf = cfg
            dataNameIcon_Map=bcConf["iconMap"]
            ipMedia=bcConf['bcNodeProfiles']['generalprofile']['resServer_ipAddress']
            portMedia = bcConf['bcNodeProfiles']['generalprofile']['resServer_port']
            for userKey, userInfo in bcConf['userInfo'].items():
                if(userInfo['ident']==selectedParticipant):
                    transactionTarget = userInfo['link']
                    transactionAccount = userInfo['account']
                    transactionPassPhrase = userInfo['passPhrase']
                    contractAddress = bcConf['contract']['address']
                    contractABI = bcConf['contract']['abi']

                    w3 = Web3(Web3.HTTPProvider(transactionTarget, request_kwargs={'timeout': 60}))
                    w3.eth.defaultAccount = transactionAccount
                    w3.personal.unlockAccount(account=w3.eth.defaultAccount,passphrase=transactionPassPhrase)

                    contractInstance = w3.eth.contract(
                        address=contractAddress,
                        abi=contractABI,
                    )
                    logFormatDict = {}
                    for interfaceFormat in contractABI[:]:
                        logFormatDict.update({interfaceFormat['name']:interfaceFormat})
                    try:
                        [nextIndex, dataPayee, dataPrice, dataName, dataHash, dataUrl, dataTotal] = contractInstance.functions.getDataRecord(bytes(dataName,'utf-8')).call()
                    except Exception as e:
                        pass
                    tx_hash = contractInstance.functions.dataBuy(w3.eth.defaultAccount, dataName).transact({'from': transactionAccount, 'gas': 1000000, 'value': w3.toWei(dataPrice,'ether')})
                    receipt = w3.eth.waitForTransactionReceipt(tx_hash)
                    logs = contractInstance.events.databuy_event().processReceipt(receipt)
                    result = logs[0].args.databuy
                    print("********************************************************")
                    if result:
                        print("Successfully purchasing Data")
                    else:
                        print("Failed to purchase Data")
                    print("********************************************************")
                    break
        except Exception:
            traceback.print_exc()
            return HttpResponse(json.dumps({'result': 'Error'})) # incorrect post
        return_data = {
            'result': result,
        }
        print(json.dumps(return_data))
        return HttpResponse(json.dumps(return_data))
    else:
        raise Http404

def ajax_dataPurchaserListQuery(request):
    if request.is_ajax():
        try:
            dataName    =  request.POST['dataName']
            datapayee    =  request.POST['payee']
            print("ajax_dataPurchaserListQuery request on : ", dataName)

            confFile = Path(os.path.join(PROJECT_ROOT, 'conf.bin'))
            cfg={}
            if confFile.exists():
                json_data=open('conf.bin').read()
                cfg = json.loads(json_data)
            bcConf = cfg
            dataNameIcon_Map=bcConf["iconMap"]
            ipMedia=bcConf['bcNodeProfiles']['generalprofile']['resServer_ipAddress']
            portMedia = bcConf['bcNodeProfiles']['generalprofile']['resServer_port']

            userInfo = bcConf['userInfo'][datapayee]

            transactionTarget = userInfo['link']
            transactionAccount = userInfo['account']
            transactionPassPhrase = userInfo['passPhrase']
            contractAddress = bcConf['contract']['address']
            contractABI = bcConf['contract']['abi']

            w3 = Web3(Web3.HTTPProvider(transactionTarget, request_kwargs={'timeout': 60}))
            w3.eth.defaultAccount = transactionAccount
            w3.personal.unlockAccount(account=w3.eth.defaultAccount,passphrase=transactionPassPhrase)

            contractInstance = w3.eth.contract(
                address=contractAddress,
                abi=contractABI,
            )
            logFormatDict = {}
            for interfaceFormat in contractABI[:]:
                logFormatDict.update({interfaceFormat['name']:interfaceFormat})

            index = 0 #traverse from list's starting point
            result = []
            while True:
                try:
                    [nextIndex, buyer] = contractInstance.functions.getAddressRecord(index, bytes(dataName,'utf-8')).call()
                except Exception as e:
                    #traceback.print_exc()
                    break
                if nextIndex != 0:
                    dataPayeeIdent = None
                    for uKey, uInfo in bcConf['userInfo'].items():
                        if(uInfo['account']==buyer):
                            dataPayeeIdent = uInfo['ident']
                            break
                    result.append([nextIndex, dataPayeeIdent, "http://"+ipMedia+":"+portMedia+"/"+dataNameIcon_Map[dataPayeeIdent]])
                else:
                    break
                index = nextIndex
        except Exception as e:
            return HttpResponse(json.dumps({'result': 'Error'})) # incorrect post
        return_data = result
        print(json.dumps(return_data))
        return HttpResponse(json.dumps(return_data))
    else:
        raise Http404

def ajax_dataPurchaseVerify(request):
    if request.is_ajax():
        try:
            dataName    =  request.POST['dataName']
            selectedParticipant    =  request.POST['selectedParticipant']
            dataPayee    =  request.POST['payee']
            print("ajax_dataPurchaseVerify request on : ", dataName,"(",dataPayee,")", selectedParticipant)

            confFile = Path(os.path.join(PROJECT_ROOT, 'conf.bin'))
            cfg={}
            if confFile.exists():
                json_data=open('conf.bin').read()
                cfg = json.loads(json_data)
            bcConf = cfg
            dataNameIcon_Map=bcConf["iconMap"]
            ipMedia=bcConf['bcNodeProfiles']['generalprofile']['resServer_ipAddress']
            portMedia = bcConf['bcNodeProfiles']['generalprofile']['resServer_port']
            for userKey, userInfo in bcConf['userInfo'].items():
                if(userInfo['ident']==dataPayee):
                    transactionTarget = userInfo['link']
                    transactionAccount = userInfo['account']
                    transactionPassPhrase = userInfo['passPhrase']
                    contractAddress = bcConf['contract']['address']
                    contractABI = bcConf['contract']['abi']

                    w3 = Web3(Web3.HTTPProvider(transactionTarget, request_kwargs={'timeout': 60}))
                    w3.eth.defaultAccount = transactionAccount
                    w3.personal.unlockAccount(account=w3.eth.defaultAccount,passphrase=transactionPassPhrase)

                    contractInstance = w3.eth.contract(
                        address=contractAddress,
                        abi=contractABI,
                    )
                    logFormatDict = {}
                    for interfaceFormat in contractABI[:]:
                        logFormatDict.update({interfaceFormat['name']:interfaceFormat})

                    tx_hash = contractInstance.functions.verifyBuyer(bcConf['userInfo'][selectedParticipant]['account'], bytes(dataName,'utf-8')).transact({'gas':3000000})
                    receipt = w3.eth.waitForTransactionReceipt(tx_hash)
                    logs = contractInstance.events.verifybuyer_event().processReceipt(receipt)
                    result = None
                    #print(logs)
                    try:
                        result = logs[0].args.verifybuyer
                    except Exception as e:
                        traceback.print_exc()
                    print("********************************************************")
                    if result:
                        print("Buyer is verified")
                    else:
                        print("Buyer is not verified")
                    print("********************************************************")
        except Exception:
            traceback.print_exc()
            return_data = {
                'result': False,
            }
            return HttpResponse(json.dumps(return_data))
        return_data = {
            'result': result
        }
        return HttpResponse(json.dumps(return_data))
    else:
        raise Http404

def ajax_purchaserHistoryQuery(request):
    if request.is_ajax():
        try:
            purchaserIdent    =  request.POST['purchaserIdent']
            print("ajax_purchaserHistoryQuery request on : ", purchaserIdent)
            confFile = Path(os.path.join(PROJECT_ROOT, 'conf.bin'))
            cfg={}
            if confFile.exists():
                json_data=open('conf.bin').read()
                cfg = json.loads(json_data)
            bcConf = cfg
            dataNameIcon_Map=bcConf["iconMap"]
            ipMedia=bcConf['bcNodeProfiles']['generalprofile']['resServer_ipAddress']
            portMedia = bcConf['bcNodeProfiles']['generalprofile']['resServer_port']

            if purchaserIdent in bcConf['userInfo']:
                userInfo = bcConf['userInfo'][purchaserIdent]

                transactionTarget = userInfo['link']
                transactionAccount = userInfo['account']
                transactionPassPhrase = userInfo['passPhrase']
                contractAddress = bcConf['contract']['address']
                contractABI = bcConf['contract']['abi']

                w3 = Web3(Web3.HTTPProvider(transactionTarget, request_kwargs={'timeout': 60}))
                w3.eth.defaultAccount = transactionAccount
                w3.personal.unlockAccount(account=w3.eth.defaultAccount,passphrase=transactionPassPhrase)

                contractInstance = w3.eth.contract(
                    address=contractAddress,
                    abi=contractABI,
                )
                logFormatDict = {}
                for interfaceFormat in contractABI[:]:
                    logFormatDict.update({interfaceFormat['name']:interfaceFormat})

                index = 0 #traverse from list's starting point
                result = []
                while True:
                    try:
                        [nextIndex, buyer, dataName, dataHash, dataUrl] = contractInstance.functions.getNextPurchaserHistoryElem(index, userInfo['account']).call()
                        [nextIndex, buyer, dataName, dataHash, dataUrl] = contractInstance.functions.getNextHistoryElem(index).call()
                    except Exception as e:
                        #traceback.print_exc()
                        break

                    if nextIndex != 0:
                        userName = None
                        if buyer == userInfo['account']:
                            for key, info in bcConf['userInfo'].items():
                                if buyer == info['account']:
                                    userName = info['name']
                                    break
                            result.append([nextIndex, userName, dataName.decode('UTF-8'), dataHash.decode('UTF-8'), dataUrl.decode('UTF-8')])
                    else:
                        break
                    index = nextIndex
            else:
                result=[]
        except Exception as e:
            traceback.print_exc()
            return HttpResponse(json.dumps({'result': 'Error'})) # incorrect post
        return_data = result
        return HttpResponse(json.dumps(return_data))
    else:
        raise Http404

def ajax_purchasingHistoryQuery(request):
    if request.is_ajax():
        try:
            selectedParticipant =  request.POST.get('selectedParticipant', False); #request.POST['selectedParticipant']
            confFile = Path(os.path.join(PROJECT_ROOT, 'conf.bin'))
            cfg={}
            if confFile.exists():
                json_data=open('conf.bin').read()
                cfg = json.loads(json_data)
            bcConf = cfg
            dataNameIcon_Map=bcConf["iconMap"]
            ipMedia=bcConf['bcNodeProfiles']['generalprofile']['resServer_ipAddress']
            portMedia = bcConf['bcNodeProfiles']['generalprofile']['resServer_port']

            for userKey, userInfo in bcConf['userInfo'].items():
                if(userInfo['ident']==selectedParticipant):
                    transactionTarget = userInfo['link']
                    transactionAccount = userInfo['account']
                    transactionPassPhrase = userInfo['passPhrase']
                    contractAddress = bcConf['contract']['address']
                    contractABI = bcConf['contract']['abi']
                    w3 = Web3(Web3.HTTPProvider(transactionTarget, request_kwargs={'timeout': 60}))
                    w3.eth.defaultAccount = transactionAccount
                    w3.personal.unlockAccount(account=w3.eth.defaultAccount,passphrase=transactionPassPhrase)

                    contractInstance = w3.eth.contract(
                        address=contractAddress,
                        abi=contractABI,
                    )
                    logFormatDict = {}
                    for interfaceFormat in contractABI[:]:
                        logFormatDict.update({interfaceFormat['name']:interfaceFormat})

                    index = 0 #traverse from list's starting point
                    result = []
                    while True:
                        try:
                            [nextIndex, buyer, dataName, dataHash, dataUrl] = contractInstance.functions.getNextHistoryElem(index).call()
                        except Exception as e:
                            #traceback.print_exc()
                            break
                        if nextIndex != 0:
                            userName = None
                            for key, info in bcConf['userInfo'].items():
                                if buyer == info['account']:
                                    userName = info['name']
                                    break
                            result.append([nextIndex, userName, dataName.decode('UTF-8'), dataHash.decode('UTF-8'), dataUrl.decode('UTF-8')])
                        else:
                            break
                        index = nextIndex
        except Exception as e:
            #traceback.print_exc()
            return HttpResponse(json.dumps({'result': 'Error'})) # incorrect post
        return_data = result
        return HttpResponse(json.dumps(return_data))
    else:
        raise Http404
