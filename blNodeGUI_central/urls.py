from django.urls import path

from . import views

app_name = 'blNodeGUI_central'

urlpatterns = [
    path('', views.index, name='index'),
    path('ajax_dataRegister', views.ajax_dataRegister, name='ajax_dataRegister'),
    path('ajax_dataPurchase', views.ajax_dataPurchase, name='ajax_dataPurchase'),
    path('ajax_dataPurchaserListQuery', views.ajax_dataPurchaserListQuery, name='ajax_dataPurchaserListQuery'),
    path('ajax_dataPurchaseVerify', views.ajax_dataPurchaseVerify, name='ajax_dataPurchaseVerify'),
    path('ajax_purchaserHistoryQuery', views.ajax_purchaserHistoryQuery, name='ajax_purchaserHistoryQuery'),
    path('ajax_purchasingHistoryQuery', views.ajax_purchasingHistoryQuery, name='ajax_purchasingHistoryQuery'),
    path('ajax_dcUserRegister', views.ajax_dcUserRegister, name='ajax_dcUserRegister'),
    path('ajax_BCUserInfoQuery', views.ajax_BCUserInfoQuery, name='ajax_BCUserInfoQuery'),
]